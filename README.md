# crowdtrace

This project intends to track mobility patterns of users based on wifi probe requests.

The firmware included is compiled withs MQTT,wifi_monitor and  wifi modules.This was last updated on July 2020. To upload the firmware following commands can be used.

```
esptool.py --port /dev/ttyUSB0 erase_flash
# upload cmd 1
esptool.py --port /dev/ttyUSB0 write_flash --flash_mode dio --flash_size 4MB 0x00000 <file>.bin 

# upload cmd 2 (tested on esp01)
esptool.py --port /dev/ttyUSB0 write_flash -fs 1MB -fm dout 0x00000 <file>.bin

esptool.py --port /dev/ttyUSB0 verify_flash --flash_mode dio --flash_size 4MB 0x00000 <file>.bin 
```
if the firmware is correctly upload you will see the following text in a serial monitor with baud rate set at 115200

![serial data sent by the firmware](docs/firmware_header.png)

# ESP8266 based nodes

### Tested ESP variants 

| Name | chip id in esptools| freq | state
--- | --- | --- | --- |
|ESP-01| ESP8266EX| 26 MHz | upload cmd 2

### log_serial

This is a serial logger which can be used with a network attached pc or a raspi 

### log_mqtt

A stand alone logger which will report back to an mqttserver via an available wifi connections.Mqtt and wifi credentials need to be changed by changing settings in *init.lua*

sample mqtt data format as follows
```
{"node":14955920,"mac":"e8:6f:38:5c:07:2a","rssi":-98,"packets":1,"net_tag":"uoc"}
```
# TO-DO

- [x] basic mqtt logging
- [x] basic serial assisted logging
- [x] include signal level in the packets with avg
- [ ] add client-ap based initial configuration
- [x] data saving scripts with influxdb
    - [ ] python implementation(
- [ ] Machine learning and location prediction 
- [ ] Final map based location update
- [ ] Documentation on how to change to HW MAC
- [ ] extend the nodes to ESP32 and add bluetooth support
